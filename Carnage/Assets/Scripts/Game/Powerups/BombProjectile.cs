﻿using Photon.Pun;
using UnityEngine;

public class BombProjectile : MonoBehaviour
{
    public int FuseLength = 200;
    public float MaxExplosionSize = 20;
    public float ExplosionSpeed = 1;
    public float ExplosionForce = 30;
    private int count = 0;

    void Update()
    {
        count++;
        if (count == 50) {
            GetComponent<Collider>().enabled = true;
        }
        if (count == FuseLength) {
            GetComponent<Collider>().enabled = true;
            Explode();
        }
    }

    private void Explode() {
        Explosion explosion = PhotonNetwork.Instantiate("Explosion Force", transform.position, transform.rotation, 0).GetComponent<Explosion>();
        explosion.Speed = ExplosionSpeed;
        explosion.MaxSize = MaxExplosionSize;
        explosion.Force = ExplosionForce;
        Destroy(gameObject);
    }
}
