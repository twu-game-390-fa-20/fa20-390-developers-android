﻿using System.Threading.Tasks;

public class BoostPowerup : Powerup
{
    public float AcclerationMultiplier = 100.0f;
    public float SpeedMultiplier = 2.0f;
    public float EffectTime = 5;

    public override void OnPickup(PowerupUser player)
    {
        base.OnPickup(player);
    }

    public override void Activate(PowerupUser player)
    {
        PlayerCarController pcc = player.GetComponent<PlayerCarController>();
        StatusEffectManager manager = player.GetComponent<StatusEffectManager>();
        manager.ApplyEffect(new BoostEffect(pcc), EffectTime);
        player.ClearPowerup();
    }

}
