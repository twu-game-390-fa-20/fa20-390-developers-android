﻿public class BoostPowerupContinuous : BoostPowerup
{
    public override void Activate(PowerupUser player)
    {
        base.Activate(player);
        PlayerCarController pcc = player.GetComponent<PlayerCarController>();
        StatusEffectManager manager = player.GetComponent<StatusEffectManager>();
        manager.ApplyEffect(new BoostEffectContinuous(pcc), EffectTime);
    }
}

