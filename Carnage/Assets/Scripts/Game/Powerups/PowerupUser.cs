﻿using Photon.Pun;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class PowerupUser : MonoBehaviour
{
    private Powerup powerup = null;
    private PhotonView photonView;

    public Sprite NoPowerUpIcon;

    void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    void Update()
    {
        if (photonView.IsMine && Input.GetMouseButtonDown(0) && powerup != null)
        {
            powerup.Activate(this);
        }
    }

    public void ClearPowerup()
    {
        powerup = null;
        if (photonView.IsMine)
            ParkingGameManager.Instance.PowerupDisplay.sprite = NoPowerUpIcon;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Powerup"))
        {
            if (powerup == null)
            {
                PowerupPickup powerupPickup = other.GetComponent<PowerupPickup>();
                powerup = powerupPickup.GetPowerup();
                powerup.OnPickup(this);
                powerupPickup.PowerupWasPickedUp();

                if (photonView.IsMine)
                    ParkingGameManager.Instance.PowerupDisplay.sprite = powerup.PowerUpIcon;
            }
        }
    }
}
