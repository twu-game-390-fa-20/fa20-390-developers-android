﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleManager : MonoBehaviour
{
    public Toggle nameToggle;
    public Toggle pinToggle;
    public InputField pin;
    public InputField nameInput;


    public void ToggleChanged()
    {
        if (nameToggle.isOn && pinToggle.isOn)
        {
            LobbyManager.JoinRoom(int.Parse(pin.text));
            LobbyManager.SetNickName(nameInput.text);
        }
    }
}
