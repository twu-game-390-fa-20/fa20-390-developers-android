﻿using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class ParkingVictoryWindow : MonoBehaviour
{
    [SerializeField]
    private GameObject display;
    [SerializeField]
    private Text victoryText;

    private void Start()
    {
        display.SetActive(false);
    }

    public void DisplayVictory(Player player)
    {
        display.SetActive(true);
        victoryText.text = player.NickName + " wins!";
    }
}
