﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.

public class SaveInputField : MonoBehaviour, IDeselectHandler
{
    public void OnDeselect(BaseEventData data)
    {
        float number;
        if (gameObject.name == "VictoryCountdownMasterInput" && float.TryParse(gameObject.GetComponent<InputField>().text, out number))
            ParkingGameSettings.VictoryCountdown = number;
        else if (gameObject.name == "NumberOfTimesParkingSpotMovesMasterInput" && float.TryParse(gameObject.GetComponent<InputField>().text, out number))
        {
            ParkingGameSettings.NumberOfTimesParkingSpotMoves = number;
            GameState.ParkingSpotsLeft = (int)number;
        }
        else if (gameObject.name == "BumperReboundMasterInput" && float.TryParse(gameObject.GetComponent<InputField>().text, out number))
            RoomSettings.BumperCarRebound = number;
        else if (gameObject.name == "MaxPlayersMasterInput" && float.TryParse(gameObject.GetComponent<InputField>().text, out number))
            RoomSettings.MaxPlayers = (byte)number;
    }
}

